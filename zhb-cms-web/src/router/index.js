import Vue from 'vue'
import Router from 'vue-router'
/*import login from '@/login'*/
/*import test from '@/views/test'
import updatePassword from '@/views/updatePassword'*/
Vue.use(Router)
/*export default new Router({
	routes: [{
		path: '/',
		name: 'login',
		component: login
	}, {
		path: '/test',
		name: 'test',
		component: test
	}, {
		path: '/updatePassword',
		name: 'updatePassword',
		component: updatePassword
	}]
})*/
export default new Router({
	routes: [{
			path: '/',
			redirect: '/login'
		},
		{
			path: '/home',
			component: resolve => require(['../home.vue'], resolve),
			children: [{
					path: '/',
					component: resolve => require(['../readMe.vue'], resolve)
				},
				{
					path: '/passwordUpdate',
					component: resolve => require(['../views/updatePassword.vue'], resolve)
				}
			]
		},
		{
			path: '/login',
			component: resolve => require(['../login.vue'], resolve)
		}, {
			path: '/updatePassword',
			name: 'updatePassword',
			component: resolve => require(['../views/updatePassword.vue'], resolve)
		}
	]
})