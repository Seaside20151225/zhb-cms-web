const protocol = process.env.protocol
const base = process.env.base
const baseUrls = {
  base: protocol + base
}

const interfaces = {
  login:baseUrls.base +'/cms/user/login',
  updatePwd:baseUrls.base +'/cms/user/userPassword',
  getUserInfo:baseUrls.base +'/cms/user/userCd/{userCd}/userInfo',
  searchOrgMenus:baseUrls.base +'/cms/user/userCd/{userCd}/orgCd/{orgCd}/portalCd/{portalCd}/orgMenus'
}
export default interfaces
