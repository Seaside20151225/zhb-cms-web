import axios from 'axios'
import Vue from 'vue'
import interfaces from 'interfaces'
//http错误码处理
var handleFailStatus = function(code) {
		if(code == 403) {
			// 403没权限，跳转到'无权限页面'
			alert("暂无权限访问该页面，请联系系统管理员确认权限")
			setTimeout(function() {
				//window.top.location.href = "../../403.html"
			}, 3000)
			return true
		} else if(code == 401) {
			alert("token已失效，请重新登录")
			setTimeout(function() {
				//window.top.location.href = "../../index/index.html"
			}, 3000)
			return true
		}
		return false
	}
	//请求拦截器
axios.interceptors.request.use(function(config) {
		var Zhb_Authen_Token = GC("token");
		if(!config.headers) config.headers = {};
		//如果没有Token或者是在首页，就不加token
		if(Zhb_Authen_Token ){
			if(window.location.href.indexOf("/index/index.html") < 0) {
			//		config.headers.Zhb_Authen_Token = Zhb_Authen_Token;
			config.headers.Authorization = 'Bearer ' + Zhb_Authen_Token;
			}
		}
		/**bx_token*/
		var bx_token = 'BX_TOKEN_HERE';
		if(!config.headers.bx_token) config.headers.bx_token = bx_token;
		else console.warn('不推荐在headers中添加bx_token属性！相关信息请咨询管理员！');
		/**Cache-Control*/
		config.headers['Cache-Control'] ? !0 : config.headers['Cache-Control'] = "no-cache, no-store, max-age=0"
		return config;
	}, function(error) {
		//	alert("请求失败！");
		console.log("请求失败！");
		return Promise.reject(error);
	})
	//回调拦截器
axios.interceptors.response.use(function(response) {
	//调试时，在控制台展示返回数据
	return response;
}, function(error) {
	if(error instanceof axios.Cancel) console.log("请求已被取消！");
	else console.log("请求失败！");
	return Promise.reject(error);
})

/**常用方法————Start*/
//判断入参是否为undefined
function _isUndefined(val) {
	return typeof val === 'undefined';
}
//删除cookie
function deleteCookie(name) {
	window.document.cookie = name + "=;path=/;expires=" + (new Date("1970")).toUTCString()
}

function GC(key) {
	var arr, reg = new RegExp("(^| )" + key + "=([^;]*)(;|$)");
	arr = document.cookie.match(reg)
	if(arr){
		if(arr[2]!='undefined'){
			return unescape(arr[2]);
		}else{
			return null;
		}
	}else{
		return null;
	}
}
/**
 * @param {Object} key 键名
 */
function getCookie(key,callback){
	/*var url = interfaces.getCookie+"?key="+key
	jsonp(url,null,function(err,data){
		if(err){
			console.log(err.message)
		}else{
			if(typeof callback == "function"){
				callback(data)
			}else{
				console.log("callback不是一个方法或者未传递")
			}
		}
	})*/
}
/**
 * @param {Object} key 键名
 * @param {Object} value 值
 * @param {Object} expire 过期时间(单位:秒)
 * @param {Object} callback 返回函数
 */
function setCookie(key,value,expire,callback){
/*	var url = interfaces.setCookie+"?key="+key+"&value="+value+"&expire="+expire;
	jsonp(url,null,function(err,data){
		if(err){
			console.log(err.message)
		}else{
			if(typeof callback == "function"){
				callback(data)
			}else{
				console.log("callback不是一个方法或者未传递")
			}
		}
	})*/
}
/**常用方法————End*/
function AxiosUtils(config){
	this.config=config;
	this.axios=axios;
	this.result;
	this.then = function(callback){
		var callback=callback;
		var failure=failure;
		this.result.then(function(response) {
			callback(response);
		});
		return this;
	};
	this.catch=function(errorHander){
		var errorHander=errorHander;
		this.result.catch(function(error) {
			if(error instanceof axios.Cancel) {
				return config.cancelFallback && config.cancelFallback(error);
			}
			console.log("fail!");
			//如果没权限或token失效就跳转到相应页面
			if(!handleFailStatus(error.response.status)) {
				//如果不是，只是后台提示错误，就走失败回调
				errorHander && errorHander(error);
			}
		});
		return this;
	};
	this.request=function(){
		this.result=this.axios(config);
		return this;
	}
}
function send(config){
	return new AxiosUtils(config).request();
}
var common = {
	send: send,
	setCookie:setCookie,
	getCookie:getCookie
}
export default common